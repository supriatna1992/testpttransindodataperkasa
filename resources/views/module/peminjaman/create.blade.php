@extends('auth.dashboard')

@section('subContent')
    <div class="card-header">
        <div class="row">
            <div class="col-8 pt-2">Peminjaman Mobil</div>
            <div class="col-4 text-end"><a class="btn btn-primary" href="/peminjaman/list">Kembali</a></div>
        </div>
    </div>
    <div class="card-body">
        
        @if($tanggal_mulai && $tanggal_selesai)
        <form action={{ route('peminjaman.store') }} method="post">
            @csrf
            <div class="mb-3 row">
                <label for="tanggal_mulai"  class="col-md-4 col-form-label text-md-end text-start">Tanggal Mulai</label>
                <div class="col-md-6">
                  <input type="date" readonly value={{$tanggal_mulai}} readonly class="form-control @error('tanggal_mulai') is-invalid @enderror" id="tanggal_mulai" name="tanggal_mulai" value="{{ old('tanggal_mulai') }}">
                    @if ($errors->has('tanggal_mulai'))
                        <span class="text-danger">{{ $errors->first('tanggal_mulai') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="tanggal_selesai"  class="col-md-4 col-form-label text-md-end text-start">Tanggal Selesai</label>
                <div class="col-md-6">
                    <input type="date" readonly value={{$tanggal_selesai}} readonly class="form-control @error('tanggal_selesai') is-invalid @enderror" id="tanggal_selesai" name="tanggal_selesai" value="{{ old('tanggal_selesai') }}">
                    @if ($errors->has('tanggal_selesai'))
                        <span class="text-danger">{{ $errors->first('tanggal_selesai') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="tanggal_selesai"  class="col-md-4 col-form-label text-md-end text-start">Pilih Mobil</label>
                <div class="col-md-6">
                    @foreach ($listMobil as $item)
                        <label>
                            <input type='radio' name='mobil_id' class="@error('mobil_id') is-invalid @enderror"  value={{$item->id}} /> [{{ $item->merek }}] {{ $item->model }} {{ $item->nomor_plat }}
                        </label>
                    @endforeach
                    <br />
                    @if ($errors->has('mobil_id'))
                        <span class="text-danger">{{ $errors->first('mobil_id') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row" style="display: flex!important ">
                <center>
                    <a href="/peminjaman/create" class="col-md-3 offset-md-5 btn btn-primary m-1"> Refresh</a>
                    <input type="submit" class="col-md-3 offset-md-5 btn btn-primary m-1" value="Buat Peminjaman">
                </center>
            </div>
            
        </form>
        @else
            <form action={{ route('peminjaman.create') }} method="get">
                @csrf
                <div class="mb-3 row">
                    <label for="tanggal_mulai" class="col-md-4 col-form-label text-md-end text-start">Tanggal Mulai</label>
                        <div class="col-md-6">
                    <input type="date" class="form-control @error('tanggal_mulai') is-invalid @enderror" id="tanggal_mulai" name="tanggal_mulai" value="{{ old('merek') }}">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="tanggal_selesai" class="col-md-4 col-form-label text-md-end text-start">Tanggal Selesai</label>
                    <div class="col-md-6">
                    <input type="date" class="form-control @error('tanggal_selesai') is-invalid @enderror" id="tanggal_selesai" name="tanggal_selesai" value="{{ old('tanggal_selesai') }}">
                    </div>
                </div>

                <div class="mb-3 row">
                    <input type="submit" class="col-md-3 offset-md-5 btn btn-primary" value="Cari">
                </div>
                
            </form>
        @endif
    </div>
@endsection

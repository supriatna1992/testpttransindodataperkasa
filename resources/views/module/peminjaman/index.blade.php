@extends('auth.dashboard')

@section('subContent')
    <div class="card-header">
        <div class="row">
            <div class="col-8 pt-2">Peminjaman Mobil</div>
            <div class="col-4 text-end"><a class="btn btn-primary" href="/peminjaman/create">Buat Peminjaman</a></div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center align-middle">No</th>
                    <th class="text-center align-middle">Tanggal Mulai</th>
                    <th class="text-center align-middle">Tanggal Selesai</th>
                    <th class="text-center align-middle">Nama Peminjam</th>
                    <th class="text-center align-middle">Merek</th>
                    <th class="text-center align-middle">Nomor Plat</th>
                    <th class="text-center align-middle">Tarif Total Tarif</th>
                    <th class="text-center align-middle">Setatus</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($result as $index => $item)
                {{-- {{$item}} --}}
                    <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$item->tanggal_mulai}}</td>
                        <td>{{$item->tanggal_selesai}}</td>
                        <td>{{$item->user?->name}}</td>
                        <td>{{$item->mobil?->merek}}</td>
                        <td>{{$item->mobil?->nomor_plat}}</td>
                        <td>{{$item->tarif_total}}</td>
                        <td>{{$item->status=='isi'?"Sedang di Gunakan":"Kosong"}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table> 
    </div>
@endsection

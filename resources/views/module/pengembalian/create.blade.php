@extends('auth.dashboard')

@section('subContent')
    <div class="card-header">
        <div class="row">
            <div class="col-8 pt-2">Peminjaman Mobil</div>
            <div class="col-4 text-end"><a class="btn btn-primary" href="/pengembalian/list">Kembali</a></div>
        </div>
    </div>
    <div class="card-body">
        
        {{-- {{ $result}} --}}
        @if($nomor_plat && $result)
        <form action={{ route('pengembalian.store') }} method="post">
            @csrf
            <div class="mb-3 row">
                <label for="nomor_plat"  class="col-md-4 col-form-label text-md-end text-start">Nomor Plat</label>
                <div class="col-md-6">
                  <input type="text" readonly value="{{$nomor_plat}}" readonly class="form-control @error('nomor_plat') is-invalid @enderror" id="nomor_plat" name="nomor_plat">
                    @if ($errors->has('nomor_plat'))
                        <span class="text-danger">{{ $errors->first('nomor_plat') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="nomor_plat"  class="col-md-4 col-form-label text-md-end text-start">Peminjam</label>
                <div class="col-md-6 mt-2">
                    {{$result->user?->name}}
                </div>
            </div>
            <div class="mb-3 row">
                <label for="nomor_plat"  class="col-md-4 col-form-label text-md-end text-start">Tanggal Pinjam</label>
                <div class="col-md-6 mt-2">
                    : {{$result->tanggal_mulai}} - {{$result->tanggal_selesai}}
                </div>
            </div>
            <div class="mb-3 row">
                <label for="nomor_plat"  class="col-md-4 col-form-label text-md-end text-start">Merek </label>
                <div class="col-md-6 mt-2">
                    : {{$result->mobil?->merek}}
                </div>
            </div>
            <div class="mb-3 row">
                <label for="nomor_plat"  class="col-md-4 col-form-label text-md-end text-start">Nomor Plat</label>
                <div class="col-md-6 mt-2">
                    : {{$result->mobil?->nomor_plat}}
                </div>
            </div>
            <div class="mb-3 row" style="display: flex!important ">
                <center>
                    <a href="/pengembalian/create" class="col-md-3 offset-md-5 btn btn-primary m-1"> Refresh</a>
                    <input type="submit" class="col-md-3 offset-md-5 btn btn-primary m-1" value="Buat Pengembalian">
                </center>
            </div>
            
        </form>
        @else
            <form action={{ route('pengembalian.create') }} method="get">
                @csrf
                <div class="mb-3 row">
                    <label for="nomor_plat" class="col-md-4 col-form-label text-md-end text-start">Nomor Plat</label>
                        <div class="col-md-6">
                        <input type="text" class="form-control @error('nomor_plat') is-invalid @enderror" id="nomor_plat" name="nomor_plat" value="{{ old('merek') }}">
                    </div>
                </div>
                <div class="mb-3 row">
                    <input type="submit" class="col-md-3 offset-md-5 btn btn-primary" value="Cari">
                </div>
                
            </form>
        @endif
    </div>
@endsection

@extends('auth.dashboard')

@section('subContent')
    <div class="card-header">
        <div class="row">
            <div class="col-8 pt-2">Manajemen Mobil</div>
            <div class="col-4 text-end"><a class="btn btn-primary" href="/manajemen-mobil/create">Buat Data Mobil</a></div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Model</th>
                    <th>Type</th>
                    <th>Nomor Plat</th>
                    <th>Tarif (per)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($result as $index => $item)
                    <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$item->model}}</td>
                        <td>{{$item->merek}}</td>
                        <td>{{$item->nomor_plat}}</td>
                        <td>{{$item->tarif}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

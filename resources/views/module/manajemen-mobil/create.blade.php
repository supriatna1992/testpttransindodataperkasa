@extends('auth.dashboard')

@section('subContent')
    <div class="card-header">
        <div class="row">
            <div class="col-8 pt-2">Buat Data Mobil</div>
            <div class="col-4 text-end"><a class="btn btn-primary" href="/manajemen-mobil/list">Kembali</a></div>
        </div>
    </div>
    <div class="card-body">
        <form action={{ route('manajemen-mobil.store') }} method="post">
            @csrf
            <div class="mb-3 row">
                <label for="merek" class="col-md-4 col-form-label text-md-end text-start">merek</label>
                <div class="col-md-6">
                  <input type="text" class="form-control @error('merek') is-invalid @enderror" id="merek" name="merek" value="{{ old('merek') }}">
                    @if ($errors->has('merek'))
                        <span class="text-danger">{{ $errors->first('merek') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="model" class="col-md-4 col-form-label text-md-end text-start">Model</label>
                <div class="col-md-6">
                  <input type="text" class="form-control @error('model') is-invalid @enderror" id="model" name="model" value="{{ old('model') }}">
                    @if ($errors->has('model'))
                        <span class="text-danger">{{ $errors->first('model') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="nomor_plat" class="col-md-4 col-form-label text-md-end text-start">Nomor Plat</label>
                <div class="col-md-6">
                  <input type="text" class="form-control @error('nomor_plat') is-invalid @enderror" id="nomor_plat" name="nomor_plat" value="{{ old('nomor_plat') }}">
                    @if ($errors->has('nomor_plat'))
                        <span class="text-danger">{{ $errors->first('nomor_plat') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="tarif" class="col-md-4 col-form-label text-md-end text-start">Tarif</label>
                <div class="col-md-6">
                  <input type="text" class="form-control @error('tarif') is-invalid @enderror" id="tarif" name="tarif" value="{{ old('tarif') }}">
                    @if ($errors->has('tarif'))
                        <span class="text-danger">{{ $errors->first('tarif') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <input type="submit" class="col-md-3 offset-md-5 btn btn-primary" value="Tambahkan">
            </div>
            
        </form>
    </div>
@endsection

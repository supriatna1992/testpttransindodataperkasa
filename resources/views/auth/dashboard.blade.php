@extends('auth.layouts')

@section('content')

<div class="row justify-content-center mt-5">
    <div class="col-md-3">
        <ul class="list-group">
            <li class="list-group-item" style="cursor: pointer">
            <a href="/manajemen-mobil/list">
                    Manajemen Mobil
                </a>
                </li>
            <li class="list-group-item" style="cursor: pointer">
                <a href="/peminjaman/list">
                    Peminjaman Mobil
                </a>
                </li>
            <li class="list-group-item" style="cursor: pointer">
                <a href="/pengembalian/list">
                    Pengembalian Mobil
                </a>
                </li>
          </ul>
    </div>
    <div class="col-md-8">
        <div class="card">
            @yield('subContent')
        </div>
    </div>    
</div>
    
@endsection
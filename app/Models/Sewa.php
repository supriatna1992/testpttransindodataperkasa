<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Carbon\Carbon;
use Psy\Readline\Hoa\Console;

class Sewa extends Model
{
    use HasFactory;

    protected $table = 'trans_sewa_mobil';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'mobil_id',
        'user_id',
        'tanggal_mulai',
        'tanggal_selesai',
        'status',
    ];

    protected $appends = [
        'tarif_total'
    ];

    public function mobil():BelongsTo
    {
        return $this->belongsTo(Mobil::class, 'mobil_id');
    }

    public function user():BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getTarifTotalAttribute()
    {
        $startDate = Carbon::parse($this->tanggal_mulai);
        $endDate = Carbon::parse($this->tanggal_selesai);

        if ($endDate->isSameDay($startDate)) {
            // If the dates are the same, consider it as one day
            $numberOfDays = 1;
        } else {
            // If the dates are different, calculate the difference in days
            $numberOfDays = $endDate->diffInDays($startDate)+1;
        }
        return  $this->mobil->tarif * $numberOfDays;
    }
}

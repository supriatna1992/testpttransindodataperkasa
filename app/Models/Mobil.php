<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Mobil extends Model
{
    use HasFactory;

    protected $table = 'master_mobil';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'merek',
        'model',
        'nomor_plat',
        'tarif',
    ];
    
    protected $appends = [
        'status',
        'tanggal_mulai',
        'tanggal_selesai',
    ];

    public function sewaMobil()
    {
        return $this->hasMany(Sewa::class,'mobil_id','id');
    }

    public function getStatusAttribute()
    {
        $status = 'kosong';
        $result = Sewa::where('mobil_id', $this->id)->orderBy('id','DESC')->first();
        if($result) {
            $status =$result->status;
        }

        return $status;
    }

    public function getTanggalMulaiAttribute()
    {
        $tanggal = null;
        $result = Sewa::where('mobil_id', $this->id)->orderBy('id','DESC')->first();
        if($result) {
            $tanggal =$result->tanggal_mulai;
        }

        return $tanggal;
    }

    public function getTanggalSelesaiAttribute()
    {
        $tanggal = null;
        $result = Sewa::where('mobil_id', $this->id)->orderBy('id','DESC')->first();
        if($result) {
            $tanggal =$result->tanggal_selesai;
        }

        return $tanggal;
    }

}

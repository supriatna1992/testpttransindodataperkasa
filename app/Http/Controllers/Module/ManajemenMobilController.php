<?php

namespace App\Http\Controllers\Module;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mobil;

class ManajemenMobilController extends Controller
{
    public function index()
    {
        $result = Mobil::get();
        return view('module.manajemen-mobil.index',compact('result'));
    }

    public function create($id='')
    {
        return view('module.manajemen-mobil.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'merek' => 'required|string',
            'model' => 'required|string',
            'nomor_plat' => 'required|string',
            'tarif' => 'required'
        ]);
        Mobil::create($request->all());

        return redirect('manajemen-mobil/list');
    }
}

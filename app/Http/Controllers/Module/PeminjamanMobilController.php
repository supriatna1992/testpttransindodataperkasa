<?php

namespace App\Http\Controllers\Module;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sewa;
use Illuminate\Support\Facades\Auth;
use App\Models\Mobil;

class PeminjamanMobilController extends Controller
{
    public function index()
    {
        $result = Sewa::with('mobil','user')
        ->where('status', 'isi')
        ->get();

        return view('module.peminjaman.index', compact('result'));
    }

    public function create(Request $request)
    {
        if($request->tanggal_mulai && $request->tanggal_selesai){
            $request->validate([
                'tanggal_mulai'         => 'required|string',
                'tanggal_selesai'         => 'required|string'
            ]);   
        }
        $result = Mobil::get();

        $result = $result->filter(function ($post) use ($request) {
            return $post->tanggal_mulai == null || 
            $post->tanggal_mulai != $request->tanggal_mulai && 
            $post->tanggal_selesai != $request->tanggal_selesai &&
            $post->tanggal_selesai < $request->tanggal_selesai; // Change this condition as needed
        });

        return view('module.peminjaman.create')->with([
            'tanggal_mulai' => $request->tanggal_mulai,
            'tanggal_selesai' => $request->tanggal_selesai,
            'listMobil'=> $result,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'tanggal_mulai'         => 'required|string',
            'tanggal_selesai'       => 'required|string',
            'mobil_id'              => 'required|integer'
        ]);
        Sewa::create([
            'mobil_id'          => $request->mobil_id,
            'user_id'           => auth()->user()->id,
            'tanggal_mulai'     => $request->tanggal_mulai,
            'tanggal_selesai'   => $request->tanggal_selesai,
            'status'            => 'isi',
        ]);
        
        return redirect('/peminjaman/list');
    }
}

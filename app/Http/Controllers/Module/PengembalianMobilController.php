<?php

namespace App\Http\Controllers\Module;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sewa;

class PengembalianMobilController extends Controller
{
    public function index()
    {
        $result = Sewa::with('mobil','user')
        ->where('status', 'kosong')
        ->get();

        return view('module.pengembalian.index', compact('result'));
    }

    public function store(Request $request)
    {
        $result = Sewa::with(['mobil', 'user'])->when($nomor_plat =$request->nomor_plat,function($q) use ($nomor_plat) {
            $q->whereHas('mobil', function($q) use ($nomor_plat){
                $q->where('nomor_plat', $nomor_plat);
            });
        })->update(['status'=>'kosong']);
        return redirect('pengembalian/list');
    }

    public function create(Request $request)
    {
        if($request->nomor_plat){
            $request->validate([
                'nomor_plat'         => 'required|string',
            ]);   
        }
        $result = null;

        if($request->nomor_plat) {
            $result = Sewa::with(['mobil', 'user'])->when($nomor_plat =$request->nomor_plat,function($q) use ($nomor_plat) {
                $q->whereHas('mobil', function($q) use ($nomor_plat){
                    $q->where('nomor_plat', $nomor_plat);
                });
            })
            ->whereStatus('isi')
            ->first(); 
        }

        return view('module.pengembalian.create')->with([
            'nomor_plat' => $request->nomor_plat,
            'result'=> $result,
        ]);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trans_sewa_mobil', function (Blueprint $table) {
            $table->id();
            $table->integer('mobil_id');
            $table->integer('user_id');
            $table->date('tanggal_mulai');
            $table->date('tanggal_selesai');
            $table->string('status')->comment('kosong, isi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trans_sewa_mobil');
    }
};

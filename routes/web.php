<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginRegisterController;
use App\Http\Controllers\Module\ManajemenMobilController;
use App\Http\Controllers\Module\PeminjamanMobilController;
use App\Http\Controllers\Module\PengembalianMobilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::controller(LoginRegisterController::class)->group(function() {
    Route::get('/register', 'register')->name('register');
    Route::post('/store', 'store')->name('store');
    Route::get('/login', 'login')->name('login');
    Route::post('/authenticate', 'authenticate')->name('authenticate');
    Route::get('/dashboard', 'dashboard')->name('dashboard');
    Route::post('/logout', 'logout')->name('logout');
});

Route::controller(ManajemenMobilController::class)->group(function() {
    Route::get('/manajemen-mobil/list', 'index')->name('manajemen-mobil.index');
    Route::get('/manajemen-mobil/create', 'create')->name('manajemen-mobil.create');
    Route::post('/manajemen-mobil/store', 'store')->name('manajemen-mobil.store');
});

Route::controller(PeminjamanMobilController::class)->group(function() {
    Route::get('/peminjaman/list', 'index')->name('peminjaman.index');
    Route::get('/peminjaman/create', 'create')->name('peminjaman.create');
    Route::post('/peminjaman/store', 'store')->name('peminjaman.store');
});

Route::controller(PengembalianMobilController::class)->group(function() {
    Route::get('/pengembalian/list', 'index')->name('pengembalian.index');
    Route::get('/pengembalian/create', 'create')->name('pengembalian.create');
    Route::post('/pengembalian/store', 'store')->name('pengembalian.store');
});